
use tower_web::{ Response, impl_web, derive_resource, impl_web_clean_top_level, impl_web_clean_nested, derive_resource_impl, ServiceBuilder};
use rand::{ thread_rng, seq::SliceRandom };
use serde_derive::{self, Serialize};

const INDEX:  &str = "^(;,;)^";

const GRAPHICS: [&str; 8] = [
    "(╯°Д°）╯︵/(.□ . \\)",
    "(˚Õ˚)ر ~~~~╚╩╩╝",
    "ヽ(ຈل͜ຈ)ﾉ︵ ┻━┻",
    "(ノಠ益ಠ)ノ彡┻━┻",
    "(╯°□°）╯︵ ┻━┻",
    "(┛◉Д◉)┛彡┻━┻",
    "┻━┻︵ \\(°□°)/ ︵ ┻━┻",
    "(┛ಠ_ಠ)┛彡┻━┻",
];

const IN_CHANNEL: &str = "in_channel";

#[derive(Clone, Debug)]
pub struct Flip {}

/// This type will be part of the web service as a resource.
#[derive(Clone, Debug, Response)]
pub struct FlipResponse {
    response_type: &'static str,
    text: &'static str,
    attachments: Vec<String>,
}

impl_web! {
    impl Flip {

        #[get("/")]
        fn index(&self) -> Result<&'static str, ()> {
            Ok(INDEX)
        }

        #[post("/flip")]
        #[content_type("application/json")]
        fn flip(&self, body: Vec<u8>) -> Result<FlipResponse, ()> {
            // You can also respond with an owned `String`.
            let _ = body.len();
            let mut rng = thread_rng();
            GRAPHICS.choose(&mut rng).map(|v| FlipResponse { response_type: IN_CHANNEL, text: *v, attachments: Vec::new()}).ok_or(())
        }
    }
}

pub fn main() {
    // Next, we must run our web service.
    //
    // The HTTP service will listen on this address and port.
    let addr = "0.0.0.0:8000".parse().expect("Invalid address");
    println!("Listening on http://{}", addr);

    // A service builder is used to configure our service.
    ServiceBuilder::new()
        // We add the resources that are part of the service. In this example,
        // there is only a single resource.
        .resource(Flip {})
        // We run the service
        .run(&addr)
        .unwrap();
}

